/*
    While sleep function.
    Usage:
        function funcName {
            // Code
            sleep(0.2);
            // More code
        }
*/
function sleep(seconds) {
    var d = Date.now();
    var cd = null;
    do cd = Date.now();
    while(cd - d < seconds * 1000);
}

/*
    Async/Await sleep function.
    Usage:
        async function funcName() {
            // Code
            await sleep_AA(0.2);
            // More code
        }
*/
function sleep_AA(seconds){
    return new Promise((res, rej) => {
        setTimeout(res, seconds * 1000);
    });
}